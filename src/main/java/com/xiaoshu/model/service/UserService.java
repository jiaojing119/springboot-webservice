package com.xiaoshu.model.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.xiaoshu.model.User;

@WebService(
		endpointInterface = "com.xiaoshu.model.service.UserService", 
	    serviceName="userService", 
		targetNamespace="http://impl.service.model.xiaoshu.com/")
public interface UserService {

	@WebMethod
	String getName(@WebParam(name = "userId") Long userId);

	@WebMethod
	User getUser(Long userId);

}
